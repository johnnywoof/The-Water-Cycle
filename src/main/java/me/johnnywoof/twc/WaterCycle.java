package me.johnnywoof.twc;

import org.bukkit.World;
import org.bukkit.entity.Villager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.concurrent.ThreadLocalRandom;

public class WaterCycle extends JavaPlugin implements Runnable {

    private int timerInterval;
    private int maxHeight;

    @Override
    public void onEnable() {

        this.saveDefaultConfig();

        this.timerInterval = this.getConfig().getInt("task-timer-interval", 60);
        this.maxHeight = this.getConfig().getInt("max-height", 300);

        this.getServer().getScheduler().runTaskTimer(this, this, 100, this.timerInterval * 20);

    }

    @Override
    public void run() {

        this.getServer().getWorlds().stream()
                .filter(world -> !world.hasStorm() && isDay(world))
                .forEach(world -> world.getEntitiesByClass(Villager.class).stream()
                        .filter(e -> e.getLocation().getY() < this.maxHeight && (!e.isOnGround() || ThreadLocalRandom.current().nextInt(5) == 2))
                        .forEach(e -> e.addPotionEffect(new PotionEffect(
                                PotionEffectType.LEVITATION,
                                (ThreadLocalRandom.current().nextInt(2, 5) + this.timerInterval) * 20,
                                ThreadLocalRandom.current().nextInt(1, 3),
                                true,
                                false
                        ), true)));

    }

    private boolean isDay(World world) {
        long time = world.getTime();
        return time < 12300 || time > 23850;
    }

}
